package com.androidlazylog.processor;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

import com.androidlazylog.Trace;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;

@SuppressWarnings("restriction")
@SupportedAnnotationTypes({ "com.androidlazylog.Trace" })
@SupportedSourceVersion(SourceVersion.RELEASE_6)
public class LazyLogProcessor extends AbstractProcessor {

    private JavacElements elementUtils;
    private TreeMaker maker;

    @Override
    public void init(ProcessingEnvironment env) {
        JavacProcessingEnvironment javacProcessingEnv = (JavacProcessingEnvironment) env;
        elementUtils = javacProcessingEnv.getElementUtils();
        maker = TreeMaker.instance(javacProcessingEnv.getContext());
    }

    @Override
    public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env) {

        for (Element element : env.getElementsAnnotatedWith(Trace.class)) {
            JCTree.JCMethodDecl methodNode = (JCTree.JCMethodDecl) elementUtils.getTree(element);
            
            
        }

        return false;
    }
}
